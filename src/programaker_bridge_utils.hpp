#ifndef PROGRAMAKER_BRIDGE_UTILS_HPP
#define PROGRAMAKER_BRIDGE_UTILS_HPP

#include <nlohmann/json.hpp>
using json = nlohmann::json;

namespace programaker {
    int json_to_int(json value);
    float json_to_float(json value);
    bool json_to_bool(json value);
    const string json_to_string(json value);
}

#endif
