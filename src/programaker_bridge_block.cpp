#include "programaker_bridge_block.hpp"

namespace programaker {

    string serialize_type(BlockArgumentType type) {
        switch(type) {
        case INTEGER:
            return "integer";
        case STRING:
            return "string";
        case FLOAT:
            return "float";
        case BOOLEAN:
            return "boolean";
        case VARIABLE:
            return "variable";

        default:
            throw std::runtime_error("Unexpected type");
        }
    }

    BridgeBlock::BridgeBlock(string id,
                             string function_name,
                             string block_type,
                             // "block_result_type": null,
                             string message,
                             std::list<struct bridge_block_argument> arguments,
                             std::function<json(std::vector<json>)> callback
        ){

        this->id = id;
        this->function_name = function_name;
        this->block_type = block_type;
        this->message = message;
        this->arguments = arguments;
        this->callback = callback;
    }

    json BridgeBlock::serialize() const {
        json arguments(json::value_t::array);
        for (auto const& argument : this->arguments) {
            auto type = serialize_type(argument.type);

            arguments.push_back({
                    { "type", type },
                    { "default", argument._defaultOrClass },
                });
        }

        json msg = {
            { "id", this->id},
            { "function_name", this->function_name },
            { "block_type", this->block_type },
            { "block_result_type", { /* NULL */ } },
            { "message", this->message },
            { "arguments", arguments },
        };

        return msg;
    }

    BridgeSignalBlock::BridgeSignalBlock(string id,
                                         string function_name,
                                         string message,
                                         std::list<struct bridge_block_argument> arguments,
                                         int save_to
        ){

        this->id = id;
        this->function_name = function_name;
        this->message = message;
        this->arguments = arguments;
        this->save_to = save_to;
    }

    json BridgeSignalBlock::serialize() const {
        json arguments(json::value_t::array);
        for (auto const& argument : this->arguments) {
            auto type = serialize_type(argument.type);

            if (argument.type == BlockArgumentType(VARIABLE)) {
                arguments.push_back({
                        { "type", type },
                        { "class", argument._defaultOrClass },
                    });
            }
            else {
                arguments.push_back({
                        { "type", type },
                        { "default", argument._defaultOrClass },
                    });
            }
        }

        json msg = {
            { "id", this->id},
            { "function_name", this->function_name },
            { "block_type", "trigger" },
            { "key", this->id },
            { "message", this->message },
            { "arguments", arguments },
            { "save_to", {} },
            { "expected_value", {} },
        };

        if (this->save_to >= 0) {
            msg["save_to"] = {
                { "type", "argument" },
                { "index", this->save_to },
            };
        }

        return msg;
    }
}
