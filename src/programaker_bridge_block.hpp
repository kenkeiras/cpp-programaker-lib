#ifndef PROGRAMAKER_BRIDGE_BLOCK_HPP
#define PROGRAMAKER_BRIDGE_BLOCK_HPP

#include <list>
#include <functional>
#include <nlohmann/json.hpp>

using json = nlohmann::json;
using string = std::string;

namespace programaker {

    typedef enum block_argument_type {
        VARIABLE,
        STRING,
        INTEGER,
        FLOAT,
        BOOLEAN,
    } BlockArgumentType;

    struct bridge_block_argument {
        BlockArgumentType type;
        string _defaultOrClass;
    };

    class BridgeBlock {
    public:
        BridgeBlock(string id,
                    string function_name,
                    string block_type,
                    string message,
                    std::list<bridge_block_argument> arguments,
                    std::function<json(std::vector<json>)> callback
            );

        json serialize() const;

        std::function<json(std::vector<json>)> callback;
        string id;

    private:
        string function_name;
        string block_type; // "<operation | getter>",
        string message;
        std::list<bridge_block_argument> arguments;
    };

    class BridgeSignalBlock{
    public:
        BridgeSignalBlock(string id,
                          string function_name,
                          string message,
                          std::list<bridge_block_argument> arguments,
                          int save_to);

        json serialize() const;

    private:
        string id;
        string message;
        string function_name;
        int save_to;
        std::list<bridge_block_argument> arguments;
    };
}
#endif
