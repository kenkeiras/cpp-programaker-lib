#include "programaker_bridge_builder.hpp"
#include <memory>

namespace programaker {
    BridgeBuilder::BridgeBuilder() {
        this->blocks = std::list<BridgeBlock>();
        this->signal_blocks = std::list<BridgeSignalBlock>();
    }

    BridgeBuilder* BridgeBuilder::set_address(std::string address) {
        this->address = address;

        return this; // Builder pattern
    }

    BridgeBuilder* BridgeBuilder::set_name(std::string name) {
        this->name = name;

        return this; // Builder pattern
    }

    BridgeBuilder* BridgeBuilder::add_block(BridgeBlock block) {
        this->blocks.push_back(block);

        return this; // Builder pattern
    }

    BridgeBuilder* BridgeBuilder::add_getter(string id,
                                             string function_name,
                                             // "block_result_type": null,
                                             string message,
                                             std::list<struct bridge_block_argument> arguments,
                                             std::function<json(std::vector<json>)> callback) {
        return this->add_block(BridgeBlock(id, function_name, "getter", message, arguments, callback));
    }

    BridgeBuilder* BridgeBuilder::add_getter(string id,
                                             // "block_result_type": null,
                                             string message,
                                             std::list<struct bridge_block_argument> arguments,
                                             std::function<json(std::vector<json>)> callback) {
        return this->add_block(BridgeBlock(id, id, "getter", message, arguments, callback));
    }

    BridgeBuilder* BridgeBuilder::add_operation(string id,
                                                string function_name,
                                                string message,
                                                std::list<struct bridge_block_argument> arguments,
                                                std::function<json(std::vector<json>)> callback) {
        return this->add_block(BridgeBlock(id, function_name, "operation", message, arguments, callback));
    }

    BridgeBuilder* BridgeBuilder::add_operation(string id,
                                                string message,
                                                std::list<struct bridge_block_argument> arguments,
                                                std::function<json(std::vector<json>)> callback) {
        return this->add_block(BridgeBlock(id, id, "operation", message, arguments, callback));
    }

    BridgeBuilder* BridgeBuilder::add_signal(string id,
                                             string function_name,
                                             string message,
                                             std::list<struct bridge_block_argument> arguments,
                                             int save_to) {
        this->signal_blocks.push_back(BridgeSignalBlock(id, function_name, message, arguments, save_to));

        return this;
    }

    BridgeBuilder* BridgeBuilder::add_signal(string id,
                                             string function_name,
                                             string message,
                                             std::list<struct bridge_block_argument> arguments
        ) {
        return this->add_signal(id, function_name, message, arguments, -1);
    }

    Bridge* BridgeBuilder::build() {
        auto config_ptr = std::make_unique<BridgeConfiguration>(address, name, blocks, signal_blocks);
        return new Bridge(std::move(config_ptr));
    }
}
