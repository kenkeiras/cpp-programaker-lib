#ifndef PROGRAMAKER_BRIDGE_CONFIGURATION_HPP
#define PROGRAMAKER_BRIDGE_CONFIGURATION_HPP

#include "programaker_bridge_block.hpp"

#include <iostream>
#include <list>
#include <nlohmann/json.hpp>

using json = nlohmann::json;
using string = std::string;

namespace programaker {

    class BridgeConfiguration {
    public:
        BridgeConfiguration(string address, string name,
                            std::list<BridgeBlock> blocks,
                            std::list<BridgeSignalBlock> signal_blocks);
        string serialize();

        string address;
        std::list<BridgeBlock> blocks;
        ~BridgeConfiguration();

    private:
        string name;
        std::list<BridgeSignalBlock> signal_blocks;
    };
}


#endif
