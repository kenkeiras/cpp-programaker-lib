#ifndef PROGRAMAKER_BRIDGE_UTILS_HPP
#define PROGRAMAKER_BRIDGE_UTILS_HPP

#include <nlohmann/json.hpp>
using json = nlohmann::json;
using string = std::string;

namespace programaker {
    int json_to_int(json value) {
        if (value.is_null()) {
            return 0;
        }

        else if (value.type() == json::value_t::string) {
            const auto as_str = value.get<const json::string_t>();
            const auto val = atoi(as_str.c_str());
            return val;
        }

        return value.get<int>();
    }

    float json_to_float(json value) {
        if (value.is_null()) {
            return 0;
        }

        else if (value.type() == json::value_t::string) {
            const auto as_str = value.get<const json::string_t>();
            const auto val = atof(as_str.c_str());
            return val;
        }

        return value.get<float>();
    }

    bool json_to_bool(json value) {
        if (value.is_null()) {
            return false;
        }

        else if (value.type() == json::value_t::string) {
            const auto as_str = value.get<const json::string_t>();
            std::locale loc;

            if (std::tolower(as_str, loc) == "true") {
                return true;
            }
            else {
                return false;
            }
        }

        return value.get<bool>();
    }

    const string json_to_string(json value) {
        if (value.is_null()) {
            return "";
        }

        if (value.type() == json::value_t::string) {
            return value.get<const json::string_t>();
        }

        return value.dump();
    }
}

#endif
