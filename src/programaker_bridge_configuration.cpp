#include "programaker_bridge_configuration.hpp"

using json = nlohmann::json;
using string = std::string;

namespace programaker {
    BridgeConfiguration::BridgeConfiguration(string address, string name,
                                             std::list<BridgeBlock> blocks,
                                             std::list<BridgeSignalBlock> signal_blocks) {
        printf("Building config\n");
        this->address = address;
        this->name = name;
        this->blocks = blocks;
        this->signal_blocks = signal_blocks;
    }

    BridgeConfiguration::~BridgeConfiguration() {
        printf("Destroying config: %s\n", this->address.c_str());

    }

    string BridgeConfiguration::serialize() {
        json ser_blocks(json::value_t::array);
        for (auto const& block : this->blocks) {
            ser_blocks.push_back(block.serialize());
        }

        for (auto const& block : this->signal_blocks) {
            ser_blocks.push_back(block.serialize());
        }

        // Build configuration message
        json msg = {
            { "type", "CONFIGURATION"},
            { "value", {
                    { "blocks", ser_blocks},
                    { "is_public", false},
                    { "service_name", this->name},
                }
            }
        };

        auto serialized = msg.dump();
        printf(">> %s\n", serialized.c_str());

        return serialized;
    }
}
