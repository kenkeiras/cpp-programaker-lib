#ifndef PROGRAMAKER_BRIDGE_HPP
#define PROGRAMAKER_BRIDGE_HPP

#include "programaker_bridge_configuration.hpp"
#include <easywsclient/easywsclient.hpp>

#include <memory>
#include <functional>

namespace programaker {

    class Bridge
    {
    public:
        Bridge(std::unique_ptr<BridgeConfiguration> conf);
        void stream_signal(std::string name, const void* buffer, ssize_t count);
        void send_signal(std::string name, const json value);

        void run();
        ~Bridge();

        bool manual_connect();
        void manual_iterate(int iteration_ms);

    private:
        std::unique_ptr<BridgeConfiguration> configuration;
        std::unique_ptr<easywsclient::WebSocket> ws;
        bool connected;
        std::unordered_map<std::string, std::function<json(std::vector<json>)>> functions;

        // Protocol management
        void _handle_message(json message);
        void _handle_get_how_to_service_registration(json message);
        void _handle_registration(json message);
        void _handle_function_call(json message);
    };
} // namespace programaker


#endif
