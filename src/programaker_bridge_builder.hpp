#ifndef PROGRAMAKER_BRIDGE_BUILDER_HPP
#define PROGRAMAKER_BRIDGE_BUILDER_HPP

#include "programaker_bridge.hpp"
#include "programaker_bridge_configuration.hpp"

namespace programaker {
    class BridgeBuilder{
    public:
        BridgeBuilder();

        BridgeBuilder* set_address(std::string address);
        BridgeBuilder* set_name(std::string name);
        BridgeBuilder* add_block(BridgeBlock block);

        BridgeBuilder* add_getter(string id,
                                  string function_name,
                                  string message,
                                  std::list<struct bridge_block_argument> arguments,
                                  std::function<json(std::vector<json>)> callback);

        BridgeBuilder* add_getter(string id,
                                  string message,
                                  std::list<struct bridge_block_argument> arguments,
                                  std::function<json(std::vector<json>)> callback);

        BridgeBuilder* add_operation(string id,
                                     string function_name,
                                     string message,
                                     std::list<struct bridge_block_argument> arguments,
                                     std::function<json(std::vector<json>)> callback);

        BridgeBuilder* add_operation(string id,
                                     string message,
                                     std::list<struct bridge_block_argument> arguments,
                                     std::function<json(std::vector<json>)> callback);

        BridgeBuilder* add_signal(string id,
                                  string function_name,
                                  string message,
                                  std::list<struct bridge_block_argument> arguments);

        BridgeBuilder* add_signal(string id,
                                  string function_name,
                                  string message,
                                  std::list<struct bridge_block_argument> arguments,
                                  int save_to);


        Bridge* build();

    private:
        std::string address;
        std::string name;
        std::list<BridgeBlock> blocks;
        std::list<BridgeSignalBlock> signal_blocks;
    };
} // namespace programaker

#endif
