#include "programaker_bridge.hpp"
#include <stdio.h>
#include <stdlib.h>

#include <memory>
#include <optional>

#include <nlohmann/json.hpp>

using easywsclient::WebSocket;
using json = nlohmann::json;

namespace programaker {

    Bridge::Bridge(std::unique_ptr<BridgeConfiguration> conf)
    {
        printf("Bridge created\n");
        this->configuration = std::move(conf);
        this->connected = false;
        this->functions = std::unordered_map<std::string, std::function<json(std::vector<json>)>>();

        for (auto const& block : this->configuration->blocks) {
            this->functions.insert_or_assign(block.id, block.callback);
        }
    }

    Bridge::~Bridge()
    {
        printf("Bridge stopped\n");
    }

    void Bridge::run() {
        if (!this->connected) {
            if (!this->manual_connect()) {
                throw std::runtime_error("Error connecting to PrograMaker");
            }
        }

        while (ws->getReadyState() != WebSocket::CLOSED) {
            this->manual_iterate(99999);
        }
    }

    void Bridge::send_signal(std::string name, const json value) {
        if (!this->connected) {
            throw std::runtime_error("Not connected");
        }

        json msg = {
            { "type", "NOTIFICATION" },
            { "key", name },
            { "to_user", { /* NULL */ } },
            { "value", value },
            { "content", value },
        };

        this->ws->send(msg.dump());
        printf(".");
        fflush(stdout);
    }

    void Bridge::stream_signal(std::string name, const void* buffer, ssize_t count) {
        // Format count
        if (count < (1<<10)) {
            printf("%li", count);
        }
        else if (count < (1<<20)) {
            printf("%liK", count / (1 << 10));
        }
        else if (count < (1<<30)) {
            printf("%liM", count / (1 << 20));
        }
        else {
            printf("%liG", count / (1 << 30));
        }
    }

    // Protocol management
    bool Bridge::manual_connect() {
        printf("Connecting to: %s\n", this->configuration->address.c_str());
        auto conn = WebSocket::from_url(this->configuration->address);

        if (!conn) {
            return false;
        }

        this->ws = std::unique_ptr<WebSocket>(conn);
        this->connected = true;

        this->ws->send(this->configuration->serialize());

        return true;
    }

    void Bridge::manual_iterate(int iteration_ms) {
        WebSocket::pointer wsp = &*(this->ws); // <-- because a unique_ptr cannot be copied into a lambda

        this->ws->poll(iteration_ms);
        this->ws->dispatch([wsp, this](const std::string & message) {
                         printf("MSG: %s\n", message.c_str());
                         auto msg = json::parse(message);
                         this->_handle_message(msg);
                     });
    }

    // Message handling
    std::optional<string> try_json_string(json message, string key) {
        auto value = message[key];
        if (value.is_null()) {
            return {};
        }

        return value.get<const json::string_t>();
    }

    void Bridge::_handle_message(json message) {
        auto type = message["type"].get<const json::string_t>();

        if (type == "GET_HOW_TO_SERVICE_REGISTRATION") {
            this->_handle_get_how_to_service_registration(message);
        }
        else if (type == "REGISTRATION") {
            this->_handle_registration(message);
        }
        else if (type == "FUNCTION_CALL") {
            this->_handle_function_call(message);
        }
    }

    void Bridge::_handle_get_how_to_service_registration(json message) {
        auto message_id = try_json_string(message, "message_id");
        if (!message_id.has_value()) {
            return;
        }

        // Only accept is supported
        // @TODO support callbacks

        string response_id = *message_id;

        json response = {
            { "success", true },
            { "message_id", response_id },
        };

        this->ws->send(response.dump());
    }

    void Bridge::_handle_registration(json message) {
        auto message_id = try_json_string(message, "message_id");
        if (!message_id.has_value()) {
            return;
        }

        // Only accept is supported
        // @TODO support callbacks

        string response_id = *message_id;

        json response = {
            { "success", true },
            { "message_id", response_id },
        };

        this->ws->send(response.dump());
    }

    void Bridge::_handle_function_call(json message) {
        auto message_id = try_json_string(message, "message_id");
        if (!message_id.has_value()) {
            return;
        }

        try {
            auto function_name = try_json_string(message["value"], "function_name");
            if (!function_name.has_value()) {
                return;
            }

            auto parameters = message["value"]["arguments"].get<std::vector<json>>();
            auto result = this->functions.at(*function_name)(parameters);

            json response = {
                { "success", true },
                { "message_id", *message_id },
                { "result", result },
            };

            this->ws->send(response.dump());
        }
        catch (std::exception& e)
        {
            std::cout << "Exception: " << e.what() << std::endl;

            json response = {
                { "success", false },
                { "message_id", *message_id },
            };

            this->ws->send(response.dump());
        }
    }
}
