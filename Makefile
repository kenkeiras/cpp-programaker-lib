CXX?=g++
LD?=ld
CPREPRO?=g++ -E
PREFIX?=/usr/local
LIBS=-pthread

INCLUDE_PATHS=-I include/json/single_include/ -I include
CXXFLAGS=-pedantic -Wall -Werror -std=c++17 $(LIBS) $(INCLUDE_PATHS) -ggdb
LDFLAGS=-shared -lstdc++ $(LIBS)

OBJECT_NAME=programaker.o
OBJECT_FILE=build/$(OBJECT_NAME)
SHARED_LIB_NAME=programaker.so
HEADER_FILE_NAME=programaker.hpp

INCLUDE_OBJECTS=build/easywsclient.o
INCLUDE_HEADERS=include/json/single_include/nlohmann/json.hpp

.PHONY: all clean install library examples get-deps

all: library examples

library: build/$(SHARED_LIB_NAME) build/$(HEADER_FILE_NAME)

examples: bin/ex_random

bin/ex_random: examples/random.cpp $(OBJECT_FILE)
	$(CXX) $(CXXFLAGS) $(OBJECT_FILE) $< -o $@

clean:
	git clean -fdx build
	git clean -fdx bin
	sh -c 'cd include/easywsclient && git clean -fdx'

uninstall:
	rm $(PREFIX)/lib/$(SHARED_LIB_NAME)
	rm $(PREFIX)/include/$(HEADER_FILE_NAME)
	rm $(PREFIX)/include/nlohmann/json.hpp
	rm $(PREFIX)/include/easywsclient/easywsclient.hpp

install: library
	install build/$(SHARED_LIB_NAME) $(PREFIX)/lib/
	install build/*.hpp $(PREFIX)/include/

	# JSON
	mkdir -p $(PREFIX)/include/nlohmann/ || true
	install include/json/single_include/nlohmann/json.hpp $(PREFIX)/include/nlohmann/json.hpp

	# EasyWSClient
	mkdir -p $(PREFIX)/include/easywsclient/ || true
	install include/easywsclient/easywsclient.hpp $(PREFIX)/include/easywsclient/easywsclient.hpp

build/programaker.hpp: src/*.hpp
	cp $+ build/

build/programaker.so: build/programaker_bridge.o build/programaker_bridge_utils.o build/programaker_bridge_configuration.o build/programaker_bridge_block.o build/programaker_bridge_builder.o \
										 $(INCLUDE_OBJECTS)
	$(CXX) $(LDFLAGS) $+ -o $@

build/programaker.o: build/programaker_bridge.o build/programaker_bridge_utils.o build/programaker_bridge_configuration.o build/programaker_bridge_block.o build/programaker_bridge_builder.o \
										 $(INCLUDE_OBJECTS)
	$(LD) -r $+ -o $@

build/programaker_bridge.o: src/programaker_bridge.cpp  src/programaker_bridge.hpp src/programaker_bridge_configuration.hpp src/programaker_bridge_block.hpp $(INCLUDE_HEADERS)
	$(CXX) $(CXXFLAGS) -fPIC -c $<  -o $@

build/programaker_bridge_utils.o: src/programaker_bridge_utils.cpp  src/programaker_bridge_utils.hpp $(INCLUDE_HEADERS)
	$(CXX) $(CXXFLAGS) -fPIC -c $<  -o $@

build/programaker_bridge_configuration.o: src/programaker_bridge_configuration.cpp  src/programaker_bridge_configuration.hpp  src/programaker_bridge_block.hpp  $(INCLUDE_HEADERS)
	$(CXX) $(CXXFLAGS) -fPIC -c $<  -o $@

build/programaker_bridge_block.o: src/programaker_bridge_block.cpp  src/programaker_bridge_block.hpp  $(INCLUDE_HEADERS)
	$(CXX) $(CXXFLAGS) -fPIC -c $<  -o $@

build/programaker_bridge_builder.o: src/programaker_bridge_builder.cpp  src/programaker_bridge_builder.hpp src/programaker_bridge.hpp src/programaker_bridge_configuration.hpp src/programaker_bridge_block.hpp  $(INCLUDE_HEADERS)
	$(CXX) $(CXXFLAGS) -fPIC -c $<  -o $@

# Includes
get-deps:
	git submodule update --init --recursive .

include/easywsclient/easywsclient.o: include/easywsclient/easywsclient.cpp include/easywsclient/easywsclient.hpp
	$(CXX) $(CXXFLAGS) -fPIC -c $<  -o $@

build/easywsclient.o: include/easywsclient/easywsclient.o
	cp include/easywsclient/easywsclient.o $@
