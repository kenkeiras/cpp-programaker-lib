#include "../build/programaker.hpp"
#include <cstdlib>
#include <thread>
#include <chrono>

using namespace programaker;

void signal_fun(Bridge* bridge) {
    int i = 0;
    do {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        bridge->send_signal("each_second", i++);
    } while (true);
}

int main(int argc, char** argv){
    auto ws_address = std::getenv("BRIDGE_ENDPOINT");
    if (ws_address == NULL) {
        printf("Set BRIDGE_ENDPOINT environment variable to run the program.");
        printf(" Example: `BRIDGE_ENDPOINT=\"ws://...\" %s\n", argv[0]);
        return 0;
    }

    auto builder = new BridgeBuilder();
    auto bridge = builder
        ->set_name("Random example")
        ->set_address(ws_address)
        ->add_getter("get_random_number", // ID
                     "Get random between %1 and %2", // Message
                     { // Arguments
                         { BlockArgumentType(INTEGER), "0" },
                         { BlockArgumentType(INTEGER), "100"},
                     },
                     // Function definition
                     [](std::vector<json> params) {
                         auto min = json_to_int(params[0]);
                         auto max = json_to_int(params[1]);

                         return min + max + 1;
                     }
            )
        ->add_operation("printline",
                        "Log to console %1",
                        {
                            { BlockArgumentType(STRING), "Hello" },
                        },
                        [](std::vector<json> params) {
                            printf("Log: %s\n", json_to_string(params[0]).c_str());

                            return nullptr;
                        }
            )
        ->add_signal("each_second",
                     "each_second",
                     "Check each second",
                     {
                         // @TODO Make SINGLE available as constant
                         { BlockArgumentType(VARIABLE), "single" },
                     },
                     0
            )
        ->build();


    std::thread signaler(signal_fun, bridge);

    bridge->run();
    signaler.join();
}
